from django.urls import path

from .views.delete_all_orders import delete_all_orders
from .views.get_all_orders import getAllOrdersDetails
from .views.get_order_by_id import getOrderDetailsById
from .views.create_order import create_order
from .views.delete_order_by_id import delete_data_order_by_id
from .views.update_data_by_id import update_data_order_by_id
urlpatterns = [
    path('order/<int:pk>/', getOrderDetailsById),
    path('order/', getAllOrdersDetails),
    path('create_order/', create_order.as_view()),
    path('delete_order/', delete_all_orders),
    path('delete_order_by_id/<int:pk>/', delete_data_order_by_id),
    path('update_data_by_id/<int:pk>/', update_data_order_by_id)
]