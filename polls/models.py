from django.db import models

# Create your models here.
from django.db import models
from datetime import datetime
from django.utils import timezone
from django.conf import settings


# Create your models here.

class Order(models.Model):
    date = models.DateField(default=datetime.now)
    id_client = models.IntegerField()
    client_name = models.CharField(max_length=200)
    price = models.FloatField(max_length=200)
    description = models.CharField(max_length=200)
    color = models.CharField(max_length=50)
    quantity = models.IntegerField()
    city = models.CharField(max_length=200)
    postalcode = models.IntegerField()
    id_product = models.IntegerField()
    id_order = models.IntegerField()


class Product(models.Model):
    date = models.DateField()
    name = models.CharField(max_length=400)
    details = models.CharField(max_length=300)
    price = models.FloatField(max_length=200)
    description = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    stock = models.IntegerField()


class Client(models.Model):
    date = models.DateField()
    name = models.CharField(max_length=400)
    username = models.CharField(max_length=200)
    firstname = models.CharField(max_length=200)
    lastname = models.CharField(max_length=200)
    postalcode = models.CharField(max_length=300)
    city = models.CharField(max_length=200)
    companyname = models.CharField(max_length=200)
    password = models.CharField(max_length=500)


class Supervision(models.Model):
    request_return_code = models.IntegerField()
    query_exec_time = models.FloatField()
    nb_request = models.IntegerField(default=1)
    exception_in_code = models.TextField(null=True, blank=True)
    timestamp = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"Request {self.pk} - {self.request_return_code}"
