import json

from django.http import HttpResponse, HttpRequest, JsonResponse
from rest_framework.decorators import api_view

from ..models import Order


@api_view(["GET"])
def getAllOrdersDetails(request):
    orders = Order.objects.all()

    # Construire la liste des utilisateurs
    orders_list = []
    for order in orders:
        order_data = {
            'id_client': order.id_client,
            'client_name': order.client_name,
            'price': order.price,
            'description': order.description,
            'color': order.color,
            'quantity': order.quantity,
            'city': order.city,
            'postalcode': order.postalcode,
            'id_product': order.id_product,
            'id_order': order.id_order
            # Ajoutez d'autres champs au besoin
        }
        orders_list.append(order_data)

    # Retourner la liste au format JSON

    # Retourner la réponse avec le JSON formaté
    return JsonResponse(orders_list, safe=False)
