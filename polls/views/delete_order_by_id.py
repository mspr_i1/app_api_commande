import json
import pytest
from django.http import HttpResponse, HttpRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt

from ..models import Order


@method_decorator(csrf_exempt, name='dispatch')
def delete_data_order_by_id(request, pk: int):
    if request.method == 'GET':
        # Récupérer les données du product
        order = Order.objects.get(id=pk)
        data = {
            'id_client': order.id_client,
            'client_name': order.client_name,
            'price': order.price,
            'description': order.description,
            'color': order.color,
            'quantity': order.quantity,
            'city': order.city,
            'postalcode': order.postalcode,
            'id_product': order.id_product,
            'id_order': order.id_order


            # Ajoutez d'autres champs au besoin
        }
        return JsonResponse(data)

    elif request.method == 'POST':
        # Supprimer le product avec l'ID spécifié
        order = Order.objects.get(id=pk)
        order.delete()
        return JsonResponse({'message': 'Les données de la commande ont été supprimées'})
