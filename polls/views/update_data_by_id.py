import json
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from ..models import Order


@csrf_exempt
def update_data_order_by_id(request, pk: int):
    try:
        if request.method == 'GET':
            # Récupérer les données du product
            order = Order.objects.get(id=pk)
            data = {
                'id_client': order.id_client,
                'client_name': order.client_name,
                'price': order.price,
                'description': order.description,
                'color': order.color,
                'quantity': order.quantity,
                'city': order.city,
                'postalcode': order.postalcode,
                'id_product': order.id_product,
                'id_order': order.id_order
                # Ajoutez d'autres champs au besoin
            }
            return JsonResponse(data)

        elif request.method == 'POST':
            # Récupération des données de la requête POST
            order = Order.objects.get(id=pk)
            id_client = request.POST.get('id_client', order.id_client)
            client_name = request.POST.get('client_name', order.client_name)
            price = request.POST.get('price', order.price)
            description = request.POST.get('description', order.description)
            color = request.POST.get('color', order.color)
            quantity = request.POST.get('quantity', order.quantity)
            id_product = request.POST.get('id_product', order.id_product)
            id_order = request.POST.get('id_order', order.id_order)

            # Mettre à jour les données du product existant
            order.id_client = id_client
            order.client_name = client_name
            order.price = price
            order.description = description
            order.color = color
            order.quantity = quantity
            order.id_product = id_product
            order.id_order = id_order

            order.save()

            # Retournez une réponse de succès
            return JsonResponse({'message': 'commande mis à jour avec succès !'})
        else:
            return JsonResponse({'error': 'Méthode HTTP non autorisée'}, status=405)

    except Order.DoesNotExist:
        return JsonResponse({'error': 'commande non trouvé'}, status=404)

    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
