import json

from django.http import HttpResponse, HttpRequest, JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view

from ..models import Order


@method_decorator(csrf_exempt, name='dispatch')
def delete_all_orders(request):
    try:
        if request.method == 'POST':
            # Supprimer tous les products
            Order.objects.all().delete()
            # Rediriger vers une page de confirmation ou une autre vue
            return JsonResponse({'message': 'Toutes les commandes sont supprimer'})
    except json.JSONDecodeError:
        return JsonResponse({'error': 'Impossible de supprimer la liste des commandes, Token Incorrect'}, status=400)
