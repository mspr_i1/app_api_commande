from django.http import HttpResponse, HttpRequest, JsonResponse
from rest_framework.decorators import api_view

from ..models import Order


@api_view(["GET"])
def getOrderDetailsById(request,pk: int):
    # Recherche du Product dans la base de données
    try:
        order = Order.objects.get(id=pk)
        data = {
            'id_client': order.id_client,
            'client_name': order.client_name,
            'price': order.price,
            'description': order.description,
            'color': order.color,
            'quantity': order.quantity,
            'city': order.city,
            'postalcode': order.postalcode,
            'id_product': order.id_product,
            'id_order': order.id_order
            # Ajoutez d'autres champs au besoin
        }
        return JsonResponse(data)
    except Order.DoesNotExist:
        return HttpResponse('Commande non trouvé', status=404)
