from django.http import JsonResponse
from django.utils.decorators import method_decorator
from django.views import View
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from ..models import Order
import json


@method_decorator(csrf_exempt, name='dispatch')
class create_order(View):
    def post(self, request, *args, **kwargs):
        try:
            # Récupération des données de la requête POST
            id_client = request.POST.get('id_client', '')
            client_name = request.POST.get('client_name', '')
            price = request.POST.get('price', '')
            description = request.POST.get('description', '')
            color = request.POST.get('color', '')
            quantity = request.POST.get('quantity', '')
            city = request.POST.get('city', '')
            postalcode = request.POST.get('postalcode', '')
            id_product = request.POST.get('id_product', '')
            id_order = request.POST.get('id_order', '')

            # Création d'une instance de Client avec les données récupérées
            order = Order.objects.create(

                id_client=id_client,
                client_name=client_name,
                price=price,
                description=description,
                color=color,
                quantity=quantity,
                city=city,
                postalcode=postalcode,
                id_product=id_product,
                id_order=id_order

            )
            # Sauvegarde de l'instance dans la base de données
            order.save()
            # Retournez une réponse de succès
            return JsonResponse({'message': 'Commande créé avec succès !'})
        except json.JSONDecodeError:
            return JsonResponse({'error': 'Données JSON invalides'}, status=400)
