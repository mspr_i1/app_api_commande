from django.test import TestCase
from ..models import Order
from django.urls import reverse
from django.test.client import Client as APIOrder


class TestGetOrderByID(TestCase):
    def test_get_client_details_by_id(self):
        order = APIOrder()
        order_instance = Order.objects.create(
            id_client=21,
            price=20.8,
            description="Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            color="noir",
            quantity=2,
            city="Auxerre",
            postalcode=89000,
            id_product=19,
            id_order=17
        )

        # Crée une requête GET pour récupérer les détails du client
        response = order.get(f'/polls/order/{order_instance.id}/')

        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)

        # Affiche les données récupérées dans la console
        print(response.json())
