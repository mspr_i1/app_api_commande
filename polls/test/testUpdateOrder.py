from django.test import TestCase
from django.urls import reverse
from ..models import Order
from django.test.client import Client as APIOrder


class UpdateDataProductByIdTestCase(TestCase):
    def test_update_data_product_by_id(self):
        order = APIOrder()
        order_instance = Order.objects.create(
            id_client=21,
            client_name="Gerard Mancau",
            price=20.8,
            description="Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            color="noir",
            quantity=2,
            city="Auxerre",
            postalcode=89000,
            id_product=19,
            id_order=17
        )

        # Crée une requête GET pour récupérer les détails du product
        response = order.get(f'/polls/order/{order_instance.id}/')
        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)

        update = order.post(f'/polls/update_data_by_id/{order_instance.id}/', {
            'id_client': 2,
            'client_name': 'Jacque Risslain',
            'price': 32.10,
            'description': 'Café pur arabica, origine Thaïlande',
            'color': 'Noir',
            'quantity': 2,
            'city': 'Auxerre',
            'postalcode': 89000,
            'id_product': 16,
            'id_order': 3
        })

        self.assertEqual(update.status_code, 200)

