from django.test import TestCase
from ..models import Order
from django.test.client import Client as APIOrder


class Test_deleteAllOrders(TestCase):
    def test_delete_all_orders(self):
        Order.objects.create(
            id_client=7,
            client_name="Mike Houston",
            price=20.8,
            description="Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            color="noir",
            quantity=2,
            city="Auxerre",
            postalcode="89000",
            id_product=56,
            id_order=18
        )
        Order.objects.create(
            id_client=10,
            client_name="Charle Anston",
            price=20.8,
            description="Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            color="noir",
            quantity=2,
            city="Auxerre",
            postalcode="89000",
            id_product=48,
            id_order=36
        )
        Order.objects.create(
            id_client=21,
            client_name="John Doe",
            price=20.8,
            description="Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            color="noir",
            quantity=2,
            city="Auxerre",
            postalcode=89000,
            id_product=19,
            id_order=17
        )

        order = APIOrder()
        response = order.post(f'/polls/delete_order/')

        # Vérifie que la réponse a un statut 200 OKs
        self.assertEqual(response.status_code, 200)
