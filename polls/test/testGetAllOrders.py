from django.test import TestCase
from ..models import Order
from django.test.client import Client as APIOrder


class Test_getAllOrdersDetails(TestCase):
    def test_get_product_details_by_all(self):
        Order.objects.create(
            id_client=7,
            client_name='John Cena',
            price=20.8,
            description="Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            color="noir",
            quantity=2,
            city="Auxerre",
            postalcode=89000,
            id_product=56,
            id_order=18
        )
        Order.objects.create(
            id_client=10,
            client_name='Henry Chester',
            price=20.8,
            description="Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            color="noir",
            quantity=2,
            city="Auxerre",
            postalcode=89000,
            id_product=48,
            id_order=36
        )
        Order.objects.create(
            id_client=21,
            client_name='John Cash',
            price=20.8,
            description="Ce café est un pur arabica noir possédant des pointes fortes et allongés",
            color="noir",
            quantity=2,
            city="Auxerre",
            postalcode=89000,
            id_product=19,
            id_order=17
        )

        order = APIOrder()
        response = order.get(f'/polls/order/')

        # Vérifie que la réponse a un statut 200 OKs
        self.assertEqual(response.status_code, 200)
        self.maxDiff = None
        self.assertEqual(response.json(), [
            {'id_client': 7, 'client_name': 'John Cena', 'price': 20.8,
             'description': 'Ce café est un pur arabica noir possédant des pointes fortes et allongés',
             'color': 'noir',
             'quantity': 2, 'city': 'Auxerre','postalcode':89000,'id_product': 56, 'id_order': 18},
            {'id_client': 10, 'client_name': 'Henry Chester', 'price': 20.8,
             'description': 'Ce café est un pur arabica noir possédant des pointes fortes et allongés',
             'color': 'noir',
             'quantity': 2,'city': 'Auxerre','postalcode':89000, 'id_product': 48, 'id_order': 36},
            {'id_client': 21, 'client_name': 'John Cash', 'price': 20.8,
             'description': 'Ce café est un pur arabica noir possédant des pointes fortes et allongés',
             'color': 'noir',
             'quantity': 2,'city': 'Auxerre','postalcode':89000, 'id_product': 19, 'id_order': 17}
        ])
