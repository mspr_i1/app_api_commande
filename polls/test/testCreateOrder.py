# Create your tests here.
from datetime import datetime

from polls.models import Order
import unittest


class TestUniter(unittest.TestCase):
    def test_order(self):
        id_client = 1
        price = 20.8
        description = "Ce café est un pur arabica noir possédant des pointes fortes et allongés"
        color = "noir"
        quantity = 2
        city = 'Auxerre'
        postalcode = 89000
        id_product = 1
        id_order = 1
        order = Order.objects.create(id_client=id_client, price=price,
                                         description=description, color=color,
                                         quantity=quantity,city=city,postalcode=postalcode, id_product=id_product, id_order=id_order)
        self.assertEqual(order.id_client, id_client)
        self.assertEqual(order.price, price)
        self.assertEqual(order.description, description)
        self.assertEqual(order.color, color)
        self.assertEqual(order.quantity, quantity)
        self.assertEqual(order.city, city)
        self.assertEqual(order.postalcode, postalcode)
        self.assertEqual(order.id_product, id_product)
        self.assertEqual(order.id_order, id_order)