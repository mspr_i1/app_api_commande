from django.test import TestCase
from ..models import Order
from django.urls import reverse
from django.test.client import Client as APIOrder


class Test_create_product(TestCase):
    def test_post_create_order(self):
        # Création d'une instance de Client avec les données récupérées
        product = APIOrder()
        response = product.post(f'/polls/create_order/', {
            'id_client': 2,
            'client_name': 'Jacque Risslain',
            'price': 32.10,
            'description': 'Café pur arabica, origine Thaïlande',
            'color': 'Noir',
            'quantity': 2,
            'city': 'Auxerre',
            'postalcode':89000,
            'id_product': 16,
            'id_order': 3
        })

        # Vérifie que la réponse a un statut 200 OK
        self.assertEqual(response.status_code, 200)


        print(response.json())
        # Retournez une réponse de succès
