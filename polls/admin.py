from django.contrib import admin
from .models import Order, Product, Client
from .models import Supervision
# Register your models here.
admin.site.register(Order)
admin.site.register(Supervision)
admin.site.register(Client)
admin.site.register(Product)