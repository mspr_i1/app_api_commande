import json
import os
import pika
from django.core.management.base import BaseCommand
from polls.models import Product, Client  # assuming your models are in polls app

class Command(BaseCommand):
    def handle(self, *args, **options):
        connection = pika.BlockingConnection(pika.ConnectionParameters(
            host='10.5.10.177',
            port=5672,
            credentials=pika.PlainCredentials('guest', 'guest')
        ))
        channel = connection.channel()

        channel.queue_declare(queue='new_client_queue', durable=True)
        channel.queue_declare(queue='new_product_queue', durable=True)

        def callback(ch, method, properties, body):
            data = body.decode('utf-8')
            if method.routing_key == 'new_client_queue':
                self.process_client_data(data)
            elif method.routing_key == 'new_product_queue':
                self.process_product_data(data)

        channel.basic_consume(queue='new_client_queue', on_message_callback=callback, auto_ack=True)
        channel.basic_consume(queue='new_product_queue', on_message_callback=callback, auto_ack=True)

        print(' [*] Waiting for messages. To exit press CTRL+C')
        channel.start_consuming()

    def process_client_data(self, data):
        data = json.loads(data)
        client = Client(
            date=data['date'],
            name=data['name'],
            username=data['username'],
            firstname=data['firstname'],
            lastname=data['lastname'],
            postalcode=data['postalcode'],
            city=data['city'],
            companyname=data['companyname'],
            password=data['password']
        )
        client.save()
        print(f'Client created: {client.name}')

    def process_product_data(self, data):
        data = json.loads(data)
        product = Product(
            date=data['date'],
            name=data['name'],
            details=data['details'],
            price=data['price'],
            description=data['description'],
            color=data['color'],
            stock=data['stock']
        )
        product.save()
        print(f'Product created: {product.name}')