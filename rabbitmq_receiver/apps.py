from django.apps import AppConfig


class RabbitmqReceiverConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rabbitmq_receiver'
